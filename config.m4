PHP_ARG_ENABLE(parsec,
    [Whether to enable the Parsec extension],
    [  --enable-parsec         Enable Parsec extension support])

if test $PHP_PARSEC != "no"; then
    PHP_REQUIRE_CXX()
    PHP_SUBST(PARSEC_SHARED_LIBADD)
    PHP_ADD_LIBRARY(stdc++, 1, PARSEC_SHARED_LIBADD)
    PHP_ADD_LIBRARY(parsec-base, 1, PARSEC_SHARED_LIBADD)
    PHP_ADD_LIBRARY(parsec-mac, 1, PARSEC_SHARED_LIBADD)
    PHP_NEW_EXTENSION(parsec, parsec.cpp, $ext_shared)
fi
