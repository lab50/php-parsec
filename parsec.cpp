#include "parsec.h"
#include "zend_exceptions.h"

#include <parsec/pdp.h>

// Возвращает строку, память на которую была выделена из кучи, привязанной к запросу.
zend_string* macToStr(PDPL_T* mac)
{
    char* result;
    size_t result_len = spprintf(&result, 0, "%" PDP_PRNTF_LEV ":%" PDP_PRNTF_CAT, pdpl_lev(mac), pdpl_cat(mac));
    zend_string* result_copy = zend_string_init(result, result_len, 0);
    efree(result);
    return result_copy;
}

// Получение мандатной метки по pid. 0 = свой процесс.
// Возвращает мандатную метку в формате <уровень>:<категории>.
PHP_FUNCTION(getMac)
{
    pid_t pid = 0;
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &pid) == FAILURE)
    {
        zend_throw_exception(NULL, "zend_parse_parameters() failed", 0);
        return;
    }

    PDPL_T* mac = pdp_get_pid(pid);
    if (!mac)
    {
        zend_throw_exception(NULL, "pdp_get_pid() failed", 0);
        return;
    }
    zend_string* mac_str = macToStr(mac);
    pdpl_put(mac);
    RETURN_STRING(ZSTR_VAL(mac_str));
}

// Установка мандатной метки по pid. 0 = свой процесс.
PHP_FUNCTION(setMac)
{
    pid_t pid = 0;
    char* mac_str = NULL;
    size_t mac_str_len = 0;
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ls", &pid, &mac_str, &mac_str_len) == FAILURE)
    {
        zend_throw_exception(NULL, "zend_parse_parameters() failed", 0);
        return;
    }
    PDP_LEV_T level = 0;
    PDP_CAT_T categories = 0;
    sscanf(mac_str, "%" PDP_SCNF_LEV ":%" PDP_SCNF_CAT, &level, &categories);

    PDPL_T* mac = pdp_get_pid(pid);
    if (!mac)
    {
        zend_throw_exception(NULL, "pdp_get_pid() failed", 0);
        return;
    }
    pdpl_set_lev(mac, level);
    pdpl_set_cat(mac, categories);
    if (pdp_set_pid(pid, mac) != 0)
    {
        if (errno == ESRCH)
            zend_throw_exception(NULL, "Process not found", 0);
        else if (errno == EPERM)
            zend_throw_exception(NULL, "PARSEC_CAP_SETMAC required", 0);
        else if (errno == EINVAL)
            zend_throw_exception(NULL, "Invalid argument", 0);
        else
            zend_throw_exception(NULL, "parsec_setmac() failed", 0);
    }
    pdpl_put(mac);
}

// Получение мандатной метки для файла или папки.
PHP_FUNCTION(getFileMac)
{
    char* path = NULL;
    size_t path_len = 0;
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &path, &path_len) == FAILURE)
    {
        zend_throw_exception(NULL, "zend_parse_parameters() failed", 0);
        return;
    }

    PDPL_T* mac = pdp_get_path(path);
    if (!mac)
    {
        zend_throw_exception(NULL, "pdp_get_pid() failed", 0);
        return;
    }
    zend_string* mac_str = macToStr(mac);
    pdpl_put(mac);
    RETURN_STRING(ZSTR_VAL(mac_str));
}

static zend_function_entry parsec_functions[] = {
    PHP_FE(getMac, NULL)
    PHP_FE(setMac, NULL)
    PHP_FE(getFileMac, NULL)
    PHP_FE_END
};

zend_module_entry parsec_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_PARSEC_EXTNAME,
    parsec_functions,      /* Functions */
    NULL,
    NULL,                  /* MSHUTDOWN */
    NULL,                  /* RINIT */
    NULL,                  /* RSHUTDOWN */
    NULL,                  /* MINFO */
#if ZEND_MODULE_API_NO >= 20010901
    PHP_PARSEC_EXTVER,
#endif
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_PARSEC
extern "C" {
  ZEND_GET_MODULE(parsec)
}
#endif
