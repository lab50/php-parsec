#ifndef PARSEC_H
#define PARSEC_H

#define PHP_PARSEC_EXTNAME  "parsec"
#define PHP_PARSEC_EXTVER   "1.0"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

extern "C" {
  #include "php.h"
}

extern zend_module_entry parsec_module_entry;

#endif /* PARSEC_H */
